<?php
require_once 'dao/Conexion.php';
require_once 'modelo/Cliente.php';

class ClienteDao{
    public function selectAllCliente(){
        $conexion= new Conexion();

        $sql="SELECT * FROM cliente";
        $result=$conexion->query($sql);
        return $result->fetchAll(PDO::FETCH_CLASS,'stdClass');
    }

    public function insertCliente(Cliente $cliente){
        $conexion = new Conexion();
        $nombre=$cliente->getNombre();
        $apellido=$cliente->getApellido();
        $telefono=$cliente->getTelefono();
        $sql="INSERT INTO cliente (nombres,apellidos,telefono) VALUES ('$nombre','$apellido','$telefono')";
        
        return $conexion->query($sql);

    }
    public function deleteCliente($id){
        $conexion=new Conexion();
        $sql="DELETE FROM cliente WHERE cliente.id=$id";
        return $conexion->query($sql);


    }
    public function updateCliente($id,Cliente $cliente){
        $conexion = new Conexion();
        $nombre=$cliente->getNombre();
        $apellido=$cliente->getApellido();
        $telefono=$cliente->getTelefono();
        $sql="UPDATE cliente SET nombres='$nombre',apellidos='$apellido',telefono='$telefono' WHERE id=$id";

        return $conexion->query($sql);

    }
}