<?php
require_once 'dao/Conexion.php';
require_once 'modelo/Producto.php';

class ProductoDao{
    public function selectAllProductos(){
        $conexion= new Conexion();
        $sql="SELECT * FROM producto";
        $result=$conexion->query($sql);
        return $result->fetchAll(PDO::FETCH_CLASS,'stdClass');
    }

    public function insertProducto(Producto $producto){
        $conexion = new Conexion();
        $nombre=$producto->getNombre();
        $precio=$producto->getPrecio();
        $sql="INSERT INTO producto (nombre,precio) VALUES ('$nombre','$precio')";
        
        return $conexion->query($sql);

    }
    public function deleteProducto($id){
        $conexion=new Conexion();
        $sql="DELETE FROM producto WHERE producto.id=$id";
        return $conexion->query($sql);

        header('location: index.php?c=producto&a=listarProductos');


    }
    public function updateProducto($id,Producto $producto){
        $conexion = new Conexion();
        $nombre=$producto->getNombre();
        $precio=$producto->getPrecio();
        $sql="UPDATE producto SET nombre='$nombre',precio='$precio' WHERE id=$id";

        return $conexion->query($sql);

    }
}