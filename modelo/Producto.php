<?php
class Producto{
    private $id;
    private $nombre;
    private $precio;

    public function getIdProducto(){
       return $this->id;
    }
    public function getNombre(){
       return $this->nombre;
    }
    public function getPrecio(){
       return $this->precio;
    }

    public function setIdProducto($id){
        $this->id=$id;
    }
    public function setNombre($nombre){
        $this->nombre=$nombre;
    }
    public function setPrecio($precio){
        $this->precio=$precio;
    }




}