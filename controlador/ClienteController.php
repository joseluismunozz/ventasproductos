<?php
require_once("dao/ClienteDao.php");

class ClienteController{
    public function index(){

        require_once 'vistas/Cliente/crear.php';

    }
    public function guardar(){
        $ClienteDao=new ClienteDao();
        $newCliente= new Cliente();
        $newCliente->setNombre($_POST['cliente_nombre']);
        $newCliente->setApellido($_POST['cliente_apellido']);
        $newCliente->setTelefono($_POST['cliente_telefono']);
        $newCliente->setIdCliente(1);
    
        $id=$_POST['cliente_id'];
        if($id){
            //Se actualiza en la BD
            $ClienteDao->updateCliente($id,$newCliente);
        }else{
            //Se guardar en la BD
            $ClienteDao->insertCliente($newCliente);
        }
         header('Location: index.php?c=cliente');

    }
    public function listarCliente(){
        $clienteDao = new ClienteDao();

        $arrClientes = $clienteDao->selectAllCliente();
  
        require_once 'vistas/Cliente/listar.php';
    }
    public function eliminar(){
        $ClienteDao =new ClienteDao();

        $ClienteDao->deleteCliente($_GET['id']);
        header('Location: index.php?c=cliente&a=listarCliente');

        


    }
    







}